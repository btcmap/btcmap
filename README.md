# btcmap

View places on OpenStreetMap marked as accepting bitcoin.

## How to Browse

- Go to https://overpass-turbo.eu
- Center and zoom the map to an area you're interested in
- Add the following query
```
[out:json][timeout:25];
// gather results
(
  node["payment:bitcoin"="yes"]({{bbox}});
  way["payment:bitcoin"="yes"]({{bbox}});
  relation["payment:bitcoin"="yes"]({{bbox}});
);
// print results
out body;
>;
out skel qt;

{{style:
  node { color:red; fill-color: red;}
  way { color:blue; fill-color: blue;}
  relation { color:green; fill-color: green;}
}}
```
- Click Run (top left)


## How to Edit

TBD


## Further reading

- OSM Queries Tutorial[^3]
- Overpass QL[^4]
- Overpass API[^6]
- Overpass Turbo wiki[^5]


## Credits

- Tweet[^1] by Gigi
- overpass-turbo[^2] contributors

[^1]: https://nitter.net/dergigi/status/1494970256657428482
[^2]: https://github.com/tyrasd/overpass-turbo
[^3]: https://osm-queries.ldodds.com/tutorial/index.html
[^4]: https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL
[^5]: https://wiki.openstreetmap.org/wiki/Talk:Overpass_turbo
[^6]: https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_API_by_Example