# Setup overpass-doc

See https://github.com/ldodds/overpass-doc

On Fedora:

```
sudo dnf install ruby-devel

git clone https://github.com/ldodds/overpass-doc
cd overpass-doc
bundle install

# Result available in bin/overpass-doc
# If fails, run
# sudo gem pristine redcarpet --version 3.5.1
# and retry
```

# Run overpass-doc

Assuming a setup of:

```
$ tree -d -L 1 .
.
├── btcmap
├── overpass-doc
└── pages
```

generate with:

```
cd btcmap
./../overpass-doc/bin/overpass-doc map-queries ../pages
```